# Tennessee Eastman Plant (Luyben et al.) 
This anonymous repository contains the MATLAB/Simulink project of the Tennessee Eastman Plant (TEP) and the results of experiments executed on it.

The original Fortran code of this implementation is in the book titled "Plantwide Process Control" by Luyben et al. (ISBN-13: 978-0070067790).

# Simulation
The TEP has been a subject of study for many years and there are different implementations by multiple authors. This repository contains our MATLAB/Simulink implementation of the original Fortran code by Luyben et al.

The experiments documented here were executed exclusively on our MATLAB/Simulink implementation. To confirm the fidelity of our translation, we replicated the experiments documented in the book (i.e., dynamic response for loss of fresh feed F_{oA}) and compared the results. The following table shows the similarity between our graphs and the graphs shown in the book.

| Book graphs             |  Our graphs |
:-------------------------:|:-------------------------:
![](images/_B.png) | ![](images/_B.svg.png)
![](images/_HR.png) | ![](images/_HR.svg.png)
![](images/_Hsep.png) | ![](images/_Hsep.svg.png)
![](images/_Hstrip.png) | ![](images/_Hstrip.svg.png)
![](images/_P.png) | ![](images/_P.svg.png)
![](images/_Purge.png) | ![](images/_Purge.svg.png)
![](images/_Recycle.png) | ![](images/_Recycle.svg.png)
![](images/_TR.png) | ![](images/_TR.svg.png)
![](images/_Tsep.png) | ![](images/_Tsep.svg.png)
![](images/_Tstrip.png) | ![](images/_Tstrip.svg.png)
![](images/_xBE.png) | ![](images/_xBE.svg.png)
![](images/_yA.png) | ![](images/_yA.svg.png)
![](images/_yB.png) | ![](images/_yB.svg.png)
![](images/_yC.png) | ![](images/_yC.svg.png)

# Experiments
Our experiments focus on manipulations of sensor readings. The integrity attacks on sensors are executed according to the following approach: 

1. starting with an execution of the system without any attacks and recording the minimum (min) and maximum (max) values observed per sensor;
2. computing the difference diff=max−min for each sensor; and 
3. executing the attacks on sensors, one at a time, compromising their integrity with 4 fixed values: min, max, min−diff , and max+diff. We make sure that the values min − diff and max + diff are within the expected physical limits of each sensor (e.g., level sensors always ≥ 0).

We execute simulated attacks against all sensors and take note of the corresponding shutdown times (if any). We execute 56 attacks on each sensor: we consider 14 different plant conditions and execute 4 integrity attacks per sensor for each of them. The robustness of this control strategy allows us to run experiments under 14 different conditions: considering all disturbances in the range [1–13] and, additionally, without any disturbance in place. We execute 4 integrity attacks for each of the 10 sensors used in this control strategy. In total, we generate 56 rankings from 560 individual simulations that add up to 18,807.192 simulated hours (∼2.14 years).

Under the [experiments](/experiments/) directory we document the experments' results and screenshots of each individual experiment.



